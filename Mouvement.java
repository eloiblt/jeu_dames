import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

public class Mouvement extends AbstractAction
{
	Case caseCliquee;
	FenetreAccueil fen;
	
	public Mouvement(Case c, FenetreAccueil fen)
	{
		caseCliquee = c;
		this.fen = fen;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) 
	{	
		if ((fen.tour == "blanc" && caseCliquee.getAppartient() == "droite") || (fen.tour == "noir" && caseCliquee.getAppartient() == "gauche"))
			return;
		
		if (fen.caseDep == null)
		{
			if (peutJouer(caseCliquee))
			{
				fen.caseDep = caseCliquee;
				caseCliquee.setBorder(new LineBorder(Color.YELLOW,2));
				
				if (caseCliquee.estDame())
				{
					fen.casesJouablesDames(caseCliquee);
					fen.casesJouablesDamesMengeant(caseCliquee);
				}
				else
				{
					fen.casesJouables(caseCliquee);
					fen.casesJouablesMengeant(caseCliquee);
				}

				/*Set<Case> keySet = fen.hashtable.keySet(); 
				Iterator<Case> it = keySet.iterator(); 

				while (it.hasNext())
				{
					Case key = it.next(); 
					System.out.println("cl� : " + key + " - valeur : " + fen.hashtable.get(key));
				} */
			}
		}
		else 		//une case de d�part a �t� selectionn�e
		{
			if (caseCliqueeEstJouableDirectement(caseCliquee))
			{				
				caseCliquee.setCouleurPion(fen.caseDep.getCouleurPion());
				fen.equivalentCase(caseCliquee).setCouleurPion(fen.caseDep.getCouleurPion());
				
				fen.caseDep.setCouleurPion(fen.caseDep.getCouleurFond());
				fen.equivalentCase(fen.caseDep).setCouleurPion(fen.equivalentCase(fen.caseDep).getCouleurFond());
				
				fen.videCasesJouables();
				
				if (caseCliquee.getI() == 0)
				{
					caseCliquee.devientDame();
					fen.equivalentCase(caseCliquee).devientDame();
				}
					
				fen.changementTour();
				
				fen.repaint();
				fen.revalidate();
			}
			else 
			{
				if (!caseCliqueeEstJouableEnMangeant(caseCliquee))
				{
					fen.videCasesJouables();
					return;
				}
		        	
	        	ArrayList<ArrayList<Case>> valeurs = fen.hashtable.get(caseCliquee);
	        	ArrayList<Case> listeATraiter;
	        	
	        	if (valeurs.size() >= 2)
	        	{
		        	Color[] couleursPossibles = {Color.MAGENTA, Color.BLUE, Color.CYAN, Color.GREEN, Color.YELLOW};
		        	ArrayList<String> couleursUtilisees = new ArrayList<String>();
		        	
		        	int i = -1;
		        	for (ArrayList<Case> a : valeurs)
		        	{
		        		i += 1;
		        		couleursUtilisees.add(convertirCouleur(couleursPossibles[i]));
		        		for (Case z : a)
		        		{
		        			z.setBorder(new LineBorder(couleursPossibles[i], 2));
		        		}
		        	}
		        	
		        	// copie arraylist dans tableau
		        	int nb = couleursUtilisees.size() -1;
		        	String[] couleursJOptionPane = new String[nb+1];
		        	
		        	i = 0;
		        	while (nb >= 0)
		        	{
		        		couleursJOptionPane[i] = couleursUtilisees.get(i);
		        		i++;
		        		nb--;
		        	}
		        	
		        	int n;
		        	do 
		        	{
		        		n = JOptionPane.showOptionDialog(fen,
			        		    "Il y a plusieurs chemins possibles pour votre coup. Lequel d�sirez-vous emprunter ? ",
			        		    "Choix du chemin",
			        		    JOptionPane.DEFAULT_OPTION,
			        		    JOptionPane.QUESTION_MESSAGE,
			        		    null,
			        		    couleursJOptionPane,
			        		    couleursJOptionPane[0]);
		        	} while (n == -1);
	        		
	        		
		        	for (ArrayList<Case> a : valeurs)
		        	{
		        		for (Case z : a)
		        		{
		        			z.setBorder(null);
		        		}
		        	}
		        	
		        	listeATraiter = valeurs.get(n);
	        	}
	        	else 
	        	{
	        		listeATraiter = valeurs.get(0);
	        	}
	        	
	        	for (Case c : listeATraiter)
	        	{
	        		c.setCouleurPion(c.getCouleurFond());
	        		fen.equivalentCase(c).setCouleurPion(fen.equivalentCase(c).getCouleurFond());
	        	}
	        	
				caseCliquee.setCouleurPion(fen.caseDep.getCouleurPion());
				fen.equivalentCase(caseCliquee).setCouleurPion(fen.caseDep.getCouleurPion());
				
				fen.caseDep.setCouleurPion(fen.caseDep.getCouleurFond());
				fen.equivalentCase(fen.caseDep).setCouleurPion(fen.equivalentCase(fen.caseDep).getCouleurFond());
				
				fen.videCasesJouables();
				
				if (caseCliquee.getI() == 0)
				{
					caseCliquee.devientDame();
					fen.equivalentCase(caseCliquee).devientDame();
				}
				
				fen.changementTour();
				
				fen.repaint();
				fen.revalidate();
		       
			}
		}
	}
	
	public boolean peutJouer(Case c)		//verifie si case jouee ET si elle est de bonne couleur 
	{
		if (c.estVide())
			return false;
		else if (fen.tour == "blanc")
		{
			if (c.getCouleurPion() == fen.blanc || c.getCouleurPion() == fen.dameBlanche)
				return true;
		}
		else 
		{
			if (c.getCouleurPion() == fen.noir || c.getCouleurPion() == fen.dameNoire)
				return true;
		}
		return false;
	}
	
	public String convertirCouleur(Color c)
	{
		if (c.getRGB() == Color.MAGENTA.getRGB())
			return "Rose"; 
		
		if (c.getRGB() == Color.BLUE.getRGB())
			return "Bleu";
		
		if (c.getRGB() == Color.CYAN.getRGB())
			return "Cyan";
		
		if (c.getRGB() == Color.GREEN.getRGB())
			return "Vert";
		
		if (c.getRGB() == Color.YELLOW.getRGB())
			return "Jaune";
		
		return "pas trouv�";
	}
	
	public boolean caseCliqueeEstJouableDirectement(Case c)
	{
		return fen.casesDirectementJouables.contains(c);
	}
	
	public boolean caseCliqueeEstJouableEnMangeant(Case c)
	{
        for(Case cle: fen.hashtable.keySet())
        {
            if (cle == c)
            {
            	return true;
            }
        }
        return false;
	}
}

