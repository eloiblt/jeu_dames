import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Shape;
import java.awt.geom.Ellipse2D;

import javax.swing.BorderFactory;
import javax.swing.JButton;

public class Case extends JButton
{
	FenetreAccueil fen;;
	private Color couleurFond;
	private Color couleurPion;
	private int i;
	private int j;
	private String appartient;
	private boolean estDame = false;
	
	public Case(int i, int j, Color cFond, Color cPion, String provenance, FenetreAccueil fen)
	{
		super();
		
		this.i = i;
		this.j = j;
		couleurFond = cFond;
		couleurPion = cPion;
		appartient = provenance;
		this.fen = fen;
		
		this.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));
		this.setAction(new Mouvement(this, fen));
	}
	
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		
		g.setColor(couleurFond);
		g.fillRect(0,0,this.getWidth(),this.getHeight());
		g.setColor(couleurPion);
		g.fillOval(5, 5, this.getWidth()-10, this.getHeight()-10);
	}
	
	public Color getCouleurFond()
	{
		return couleurFond;
	}
	
	public Color getCouleurPion()
	{
		return couleurPion;
	}
	
	public void setCouleurPion(Color couleur)
	{
		couleurPion = couleur;
	}
	
	public int getI()
	{
		return i;
	}
	
	public int getJ()
	{
		return j;
	}
	
	public String getAppartient()
	{
		return appartient;
	}
	
	public void devientDame()
	{
		estDame = true;
		if (fen.tour == "blanc")
			setCouleurPion(fen.dameBlanche);
		else 
			setCouleurPion(fen.dameNoire);
	}
	
	public boolean estDame()
	{
		if (fen.tour == "blanc")
			return couleurPion == fen.dameBlanche;
		else 
			return couleurPion == fen.dameNoire;
	}
	
	public boolean estJoue()
	{
		return (couleurFond != couleurPion);
	}
	
	public boolean estVide()
	{
		return (couleurFond == couleurPion);
	}
	
	public String convertir(Color c)
	{
		if (c.equals(fen.marron))
			return "marron";
		else if (c.equals(fen.beige))
			return "beige";
		else if (c.equals(fen.noir))
			return "noir";
		else 
			return "blanc";
	}
	
	public String toString()
	{
		return String.valueOf(i) + ":" + String.valueOf(j);
			
	}
}
