import java.awt.Color;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

public class FenetreAccueil extends JFrame 
{
	Case[][] tableauGauche = new Case[10][10];
	Case[][] tableauDroite = new Case[10][10];
	Case caseDep;
	List<Case> casesDirectementJouables = new ArrayList<Case>();
	Hashtable<Case, ArrayList<ArrayList<Case>>> hashtable = new Hashtable<Case, ArrayList<ArrayList<Case>>>();
	List<Case> casesAEffacer = new ArrayList<Case>();
	
	String tour = "blanc";		
	
	Color marron = new Color(146,84,6);
	Color beige = new Color(234,160,67);
	Color blanc = Color.WHITE;
	Color noir = Color.BLACK;
	Color dameBlanche = new Color(160, 160, 160);
	Color dameNoire = new Color(64, 64, 64);
	
	public FenetreAccueil()
	{
		super();
	}

	public void initialiseFenetre()
	{
		this.setSize(1010, 500);
		this.setTitle("Composition de Dispositions");
	    this.setLocationRelativeTo(null);		
		this.setResizable(true);
	    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    this.setContentPane(creationDamiers());
	    this.setVisible(true);
	}
	
	public JPanel creationDamiers()
	{
		JPanel total = new JPanel(new GridLayout(1,2,10,0));
		JPanel damierGauche = new JPanel(new GridLayout(10,10,0,0));
		JPanel damierDroite = new JPanel(new GridLayout(10,10,0,0));
		
		Color couleurFond;
		Color couleurPion;
		
		for (int i=0 ; i<10; i++)
		{
			for (int j=0 ; j<10 ; j++)
			{
				
				if ((j+i)%2 == 0)
				{
					couleurFond = beige;
					couleurPion = beige;
				}
					
				else 
				{
					couleurFond = marron;
					if (i < 4)
						couleurPion = noir;
					else if (i > 5)
						couleurPion = blanc;
					else 
						couleurPion = marron;
				}
										
				tableauGauche[i][j] = new Case(i, j, couleurFond, couleurPion, "gauche", this);
				damierGauche.add(tableauGauche[i][j]);
			}
		}
		
		for (int i=0 ; i<10; i++)
		{
			for (int j=0 ; j<10 ; j++)
			{
				if ((j+i)%2 == 0)
				{
					couleurFond = beige;
					couleurPion = beige;
				}
					
				else 
				{
					couleurFond = marron;
					if (i < 4)
						couleurPion = blanc;
					else if (i > 5)
						couleurPion = noir;
					else 
						couleurPion = marron;
				}
										
				tableauDroite[i][j] = new Case(i, j, couleurFond, couleurPion, "droite", this);
				damierDroite.add(tableauDroite[i][j]);
			}
		}
		
		total.add(damierGauche);
		total.add(damierDroite);
		
		return total;
	}
	
	public void casesJouables(Case c) 
	{		
		int i = c.getI();
		int j = c.getJ();
		String provenance = c.getAppartient();
		
		Case[][] tableau;
		if (provenance == "gauche")
			tableau = tableauGauche;
		else 
			tableau = tableauDroite;
		
		try 
		{
			if (tableau[i-1][j-1].estVide())
			{
				tableau[i-1][j-1].setBorder(new LineBorder(Color.RED,2));
				casesDirectementJouables.add(tableau[i-1][j-1]);
			}
		}
		catch (ArrayIndexOutOfBoundsException e)
		{
		}
		
		try 
		{
			if (tableau[i-1][j+1].estVide())
			{
				tableau[i-1][j+1].setBorder(new LineBorder(Color.RED,2));
				casesDirectementJouables.add(tableau[i-1][j+1]);
			}
		}
		catch (ArrayIndexOutOfBoundsException e)
		{
		}
	}
	
	public void casesJouablesMengeant(Case c)
	{
		int i = c.getI();
		int j = c.getJ();
		String provenance = c.getAppartient();
		
		Case[][] tableau;
		if (provenance == "gauche")
			tableau = tableauGauche;
		else 
			tableau = tableauDroite;
		
		Color ennemi;
		if (tour == "blanc")
			ennemi = noir;
		else 
			ennemi = blanc;
		
		//nord ouest 
		try 
		{
			if (tableau[i-1][j-1].estJoue() && tableau[i-1][j-1].getCouleurPion().getRGB() == ennemi.getRGB() && !casesAEffacer.contains(tableau[i-1][j-1]) && tableau[i-2][j-2].estVide())
			{
				tableau[i-2][j-2].setBorder(new LineBorder(Color.RED,2));
				casesAEffacer.add(tableau[i-1][j-1]);

				ArrayList<Case> copie = new ArrayList<Case>(casesAEffacer);
				
				if (cleInexistante(tableau[i-2][j-2]))
				{
					ArrayList<ArrayList<Case>> ppal = new ArrayList<ArrayList<Case>>();
					ppal.add(copie);
					hashtable.put(tableau[i-2][j-2], ppal);
				}
				else 
				{
					ArrayList<ArrayList<Case>> valeurs = hashtable.get(tableau[i-2][j-2]);
					hashtable.remove(tableau[i-2][j-2]);
					valeurs.add(copie);
					hashtable.put(tableau[i-2][j-2], valeurs);
				}					
				casesJouablesMengeant(tableau[i-2][j-2]);
			}
		}
		catch (IndexOutOfBoundsException e)
		{
		}
		
		//nord est
		try 
		{
			if (tableau[i-1][j+1].estJoue() && tableau[i-1][j+1].getCouleurPion().getRGB() == ennemi.getRGB() && !casesAEffacer.contains(tableau[i-1][j+1]) && tableau[i-2][j+2].estVide())
			{
				tableau[i-2][j+2].setBorder(new LineBorder(Color.RED,2));
				casesAEffacer.add(tableau[i-1][j+1]);
				
				ArrayList<Case> copie = new ArrayList<Case>(casesAEffacer);
				
				if (cleInexistante(tableau[i-2][j+2]))
				{
					ArrayList<ArrayList<Case>> ppal = new ArrayList<ArrayList<Case>>();
					ppal.add(copie);
					hashtable.put(tableau[i-2][j+2], ppal);
				}
				else 
				{
					ArrayList<ArrayList<Case>> valeurs = hashtable.get(tableau[i-2][j+2]);
					hashtable.remove(tableau[i-2][j+2]);
					valeurs.add(copie);
					hashtable.put(tableau[i-2][j+2], valeurs);
				}
				casesJouablesMengeant(tableau[i-2][j+2]);
			}
		}
		catch (IndexOutOfBoundsException e)
		{
		}
		
		//sud ouest 
		try 
		{
			if (tableau[i+1][j-1].estJoue() && tableau[i+1][j-1].getCouleurPion().getRGB() == ennemi.getRGB() && !casesAEffacer.contains(tableau[i+1][j-1]) && tableau[i+2][j-2].estVide())
			{
				tableau[i+2][j-2].setBorder(new LineBorder(Color.RED,2));
				casesAEffacer.add(tableau[i+1][j-1]);
				
				ArrayList<Case> copie = new ArrayList<Case>(casesAEffacer);
				
				if (cleInexistante(tableau[i+2][j-2]))
				{
					ArrayList<ArrayList<Case>> ppal = new ArrayList<ArrayList<Case>>();
					ppal.add(copie);
					hashtable.put(tableau[i+2][j-2], ppal);
				}
				else 
				{
					ArrayList<ArrayList<Case>> valeurs = hashtable.get(tableau[i+2][j-2]);
					hashtable.remove(tableau[i+2][j-2]);
					valeurs.add(copie);
					hashtable.put(tableau[i+2][j-2], valeurs);
				}
				casesJouablesMengeant(tableau[i+2][j-2]);
			}
		}
		catch (IndexOutOfBoundsException e)
		{
		}
		
		//sud est 
		try 
		{
			if (tableau[i+1][j+1].estJoue() && tableau[i+1][j+1].getCouleurPion().getRGB() == ennemi.getRGB() && !casesAEffacer.contains(tableau[i+1][j+1]) && tableau[i+2][j+2].estVide())
			{
				tableau[i+2][j+2].setBorder(new LineBorder(Color.RED,2));
				casesAEffacer.add(tableau[i+1][j+1]);
				
				ArrayList<Case> copie = new ArrayList<Case>(casesAEffacer);
				
				if (cleInexistante(tableau[i+2][j+2]))
				{
					ArrayList<ArrayList<Case>> ppal = new ArrayList<ArrayList<Case>>();
					ppal.add(copie);
					hashtable.put(tableau[i+2][j+2], ppal);
				}
				else 
				{
					ArrayList<ArrayList<Case>> valeurs = hashtable.get(tableau[i+2][j+2]);
					hashtable.remove(tableau[i+2][j+2]);
					valeurs.add(copie);
					hashtable.put(tableau[i+2][j+2], valeurs);
				}
				casesJouablesMengeant(tableau[i+2][j+2]);
			}
		}
		catch (IndexOutOfBoundsException e)
		{
		}		
		
		try 
		{
			casesAEffacer.remove(casesAEffacer.size()-1);
		}
		catch (IndexOutOfBoundsException e)
		{
		}
	}

	public boolean cleInexistante(Case c) 
	{
        for(Case key: hashtable.keySet())
        {
            if (key == c)
            {
            	return false;
            }
        }
        return true;
	}

	public void videCasesJouables()
	{
		for (Case c : casesDirectementJouables)
		{
			c.setBorder(null);
		}
		
        for(Case key: hashtable.keySet())
        {
        	key.setBorder(null);
        }
		
		casesDirectementJouables.clear();	
		casesAEffacer.clear();
		caseDep.setBorder(null);
		caseDep = null;
		hashtable.clear();
	}
	
	public Case equivalentCase(Case c)
	{
		if (tour == "blanc")
			return tableauDroite[9 - c.getI()][9 - c.getJ()];
		else 
			return tableauGauche[9 - c.getI()][9 - c.getJ()];
	}
	
	public void changementTour()
	{
		if (tour == "blanc")
			tour = "noir";
		else 
			tour = "blanc";
	}

	public void casesJouablesDames(Case c) 
	{
		int iInitial = c.getI();
		int jInitial = c.getJ();
		String provenance = c.getAppartient();
		
		Case[][] tableau;
		if (provenance == "gauche")
			tableau = tableauGauche;
		else 
			tableau = tableauDroite;
		
		try 
		{
			int i = iInitial - 1;
			int j = jInitial - 1;
			while (tableau[i][j].estVide())
			{
				tableau[i][j].setBorder(new LineBorder(Color.RED,2));
				casesDirectementJouables.add(tableau[i][j]);
				i--;
				j--;
			}
		}
		catch (ArrayIndexOutOfBoundsException e)
		{
		}
		
		try 
		{
			int i = iInitial - 1;
			int j = jInitial + 1;
			while (tableau[i][j].estVide())
			{
				tableau[i][j].setBorder(new LineBorder(Color.RED,2));
				casesDirectementJouables.add(tableau[i][j]);
				i--;
				j++;
			}
		}
		catch (ArrayIndexOutOfBoundsException e)
		{
		}
		
		try 
		{
			int i = iInitial + 1;
			int j = jInitial - 1;
			while (tableau[i][j].estVide())
			{
				tableau[i][j].setBorder(new LineBorder(Color.RED,2));
				casesDirectementJouables.add(tableau[i][j]);
				i++;
				j--;
			}
		}
		catch (ArrayIndexOutOfBoundsException e)
		{
		}
		
		try 
		{
			int i = iInitial + 1;
			int j = jInitial + 1;
			while (tableau[i][j].estVide())
			{
				tableau[i][j].setBorder(new LineBorder(Color.RED,2));
				casesDirectementJouables.add(tableau[i][j]);
				i++;
				j++;
			}
		}
		catch (ArrayIndexOutOfBoundsException e)
		{
		}
	}

	public void casesJouablesDamesMengeant(Case c) 
	{
		int i = c.getI();
		int j = c.getJ();
		String provenance = c.getAppartient();
		
		Case[][] tableau;
		if (provenance == "gauche")
			tableau = tableauGauche;
		else 
			tableau = tableauDroite;
		
		try 
		{
			while (tableau[i-1][j-1].estVide())
			{
				i--;
				j--;
			}
			fonction(i-1, j-1, "no", tableau);
		}
		catch (ArrayIndexOutOfBoundsException e)
		{
		}
		
		try 
		{
			while (tableau[i-1][j+1].estVide())
			{
				i--;
				j++;
			}
		}
		catch (ArrayIndexOutOfBoundsException e)
		{
		}
		
		try 
		{
			while (tableau[i+1][j-1].estVide())
			{
				i++;
				j--;
			}
		}
		catch (ArrayIndexOutOfBoundsException e)
		{
		}
		
		try 
		{
			while (tableau[i+1][i+1].estVide())
			{
				i++;
				j++;
			}
		}
		catch (ArrayIndexOutOfBoundsException e)
		{
		}
	}

	public void fonction(int i, int j, String sens, Case[][] tableau) 
	{
		switch (sens)
		{
			case "no" : 
				while (tableau[i-1][j-1].estVide())
				{
					
				}
		}
	}
}
